package com.androspace.sttudent_management_system;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChooseCourseActivity extends AppCompatActivity implements View.OnClickListener, ChooseFieldFrag.OnFragmentInteractionListener {


    @BindView(R.id.but3)
    Button but3;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.choosecourseActivity);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
////        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
////        fab.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
////            }
////        });
//    }

//    public SharedPreferences.Editor sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosecoursecontent);
        ButterKnife.bind(this);
        clickListeners();

//        sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
//        sharedPreferences.putString("sharedValue1", "shared value is : 1").apply();

    }

    private void clickListeners() {
        but3.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//                case R.id.but1:
//                    startActivity(new Intent(LoginActivity.this, ChooseCourseActivity.class));
//                    break;

            case R.id.but3:
                ChooseFieldFrag fragmentTest1 = new ChooseFieldFrag();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.ok_container, fragmentTest1);

                //if addToBackStack() is not defined, on backPressed() will trigger call to the MainActivity
                fragmentTransaction.addToBackStack(this.getClass().getName());

                fragmentTransaction.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
