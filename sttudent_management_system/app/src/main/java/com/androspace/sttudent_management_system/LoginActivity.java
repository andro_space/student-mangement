package com.androspace.sttudent_management_system;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, SignupFrag.OnFragmentInteractionListener {


//    public Button but1;
//    public void init(){
//
//        but1=(Button)findViewById(R.id.but1);
//
//        but1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent1=new Intent(LoginActivity.this,ChooseCourseActivity.class);
//                startActivity(intent1);
//            }
//        });

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //    }
    @BindView(R.id.but1)
    Button but1;

    @BindView(R.id.but2)
    Button but2;

    public SharedPreferences.Editor sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logincontent);
        ButterKnife.bind(this);
        clickListeners();

//        sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
//        sharedPreferences.putString("sharedValue1", "shared value is : 1").apply();

    }

    private void clickListeners() {
        but1.setOnClickListener(this);
        but2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but1:
                startActivity(new Intent(LoginActivity.this, ChooseCourseActivity.class));
                break;

            case R.id.but2:

                SignupFrag fragmentTest = new SignupFrag();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.root_container, fragmentTest);

                //if addToBackStack() is not defined, on backPressed() will trigger call to the MainActivity
                fragmentTransaction.addToBackStack(this.getClass().getName());

                fragmentTransaction.commit();
                break;
        }
    }


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.loginactivity);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
//
//
//            }
//        });
//
//
////        init();
//
//    }

}
