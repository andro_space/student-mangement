package com.androspace.sttudent_management_system;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;

//import android.content.Intent;
//import android.content.SharedPreferences;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseFieldFrag.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseFieldFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseFieldFrag extends Fragment implements View.OnClickListener ,test_frag.OnFragmentInteractionListener,assignment_frag.OnFragmentInteractionListener{

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //    }
    @BindView(R.id.test)
    Button test;

    @BindView(R.id.assign)
    Button assign;

//    public SharedPreferences.Editor sharedPreferences;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.choosefeildFrag);
//        ButterKnife.bind(this);
//        clickListeners();
//
////        sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE).edit();
////        sharedPreferences.putString("sharedValue1", "shared value is : 1").apply();
//
//    }
//
//    private void clickListeners() {
//        test.setOnClickListener(this);
//        assign.setOnClickListener(this);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.test:
                SignupFrag fragmentTest = new SignupFrag();
                FragmentManager fragmentManager = getFragmentManager();
               android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.my_container, fragmentTest);

                //if addToBackStack() is not defined, on backPressed() will trigger call to the MainActivity
                fragmentTransaction.addToBackStack(this.getClass().getName());

                fragmentTransaction.commit();
                break;

            case R.id.assign:

                SignupFrag fragmentAssign = new SignupFrag();
                FragmentManager fragmentManager1 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager1.beginTransaction();
                fragmentTransaction2.replace(R.id.my_container, fragmentAssign);

                //if addToBackStack() is not defined, on backPressed() will trigger call to the MainActivity
                fragmentTransaction2.addToBackStack(this.getClass().getName());

                fragmentTransaction2.commit();
                break;
        }
    }

























    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ChooseFieldFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChooseFieldFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static ChooseFieldFrag newInstance(String param1, String param2) {
        ChooseFieldFrag fragment = new ChooseFieldFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.choosefeildfrag, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
